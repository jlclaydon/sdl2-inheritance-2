#include "AABB.h"
#include "Vector2f.h"

AABB::AABB()
{
	halfHeight = 0.0f;
	halfWidth = 0.0f;
	centerX = 0.0f;
	centerY = 0.0f;

}

AABB::AABB(Vector2f* position, float height, float width)
{
	init(position, height, width);
}

AABB::~AABB() {

}

void AABB::init(Vector2f* position, float height, float width)
{
	halfWidth = width / 2.0f;
	halfHeight = height / 2.0f;

	setPosition(position);
}

float AABB::getHalfHeight()
{
	return halfHeight;
}

float AABB::getHalfWidth()
{
	return halfWidth;
}

float AABB::getCenterX()
{
	return centerX;
}

float AABB::getCenterY()
{
	return centerY;
}

void AABB::setPosition(Vector2f* position)
{
	centerX = position->getX() + halfWidth;
	centerY = position->getY() + halfHeight;
}

bool AABB::intersects(AABB* other)
{
	// If the sprite has no collision box
	// then give up.
	if (other == nullptr)
		return false;
	bool hCheck = (fabs(this->getCenterX() - other->getCenterX()) < (this->getHalfWidth() + other->getHalfWidth()));
	bool vCheck = (fabs(this->getCenterY() - other->getCenterY()) <	(this->getHalfHeight() + other->getHalfHeight()));

	return hCheck && vCheck;
}